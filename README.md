# Identificar barreiras no código 

## Código HTML

```html
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Principal | Araras CoWorking</title>
    <style>
        #skip{

        }
    </style>
</head>

<body>
    <main>
        <a id="skip" href="#">Saltar para cnteudo principal</a>
        <h1>Araras CoWorking</h1>
        <h2>Espaço colaborativo para Start-Ups</h2>

        <p>Coworking é um movimento de pessoas, empresas e comunidades que buscam trabalhar e desenvolver suas vidas
            e
            negócios juntos, para crescer de forma mais rápida e colaborativa.</p>

        <img src="https://images.coworkingbrasil.org/coworkingbrasi/wp-content/uploads/2015/10/Eduardo_Macarios_100714_NEX_IMG_32372.jpg?cx=center&cy=center&scale.option=fill&w=875&cw=875&h=594&ch=594"
            alt="Imagem de ambiente espaçoso em formato de mesanino duplo, distribuidos nas laterais superiores esquerda e direita. O terreo é composto por nove ilhas compostas por mesas que podem agrupar até 4 pessoas. O ambiente possui nove equipes trabalhando em coworking no piso terreo."
            height="300px">

        <article>
            <h1>Venha para Araras CoWorking</h2>
                <p>Se você é uma pequena empresa ou um profissional independente, você pode utilizar um Espaço de
                    Coworking
                    como
                    seu escritório. Geralmente você pode contratar um plano baseado em horas de utilização ou mesmo um
                    plano
                    fixo mensal. Lá você vai encontrar toda a estrutura tradicional de um escritório, com uma boa
                    localização,
                    uma grande comunidade de profissionais diversos para trocar experiências e um custo muito inferior a
                    manutenção de um escritório próprio.</p>
                
                    <aside>Os custos de manutenção e operação são baixíssimos, solicite o contato de um dos nossos
                    representantes
                    enviando e-mail para <a
                        href="mailto:contato@dominio.com? Subject: Assunto da mensagem&body=Conteúdo da mensagem">ararascoworking@net.com</a>
                </aside>
        </article>

            <h3>Formulario de Cadastro.</h3>
            <form action="" method="post">
                <fieldset>
                    <legend>Dados pessoais</legend>
                    <label for="nome">Nome:</label>
                    <input type="text" id="nome" name="nome"><br>
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email"><br>
                    <label for="telefone">Telefone:</label>
                    <input type="tel" id="telefone" name="telefone"><br>
                    <label for="end-res">Endereço:</label>
                    <input type="text" id="end-res" name="end-res"><br>
                    <label for="num-res">Número:</label>
                    <input type="text" id="num-res" name="num-res"><br>
                    <label for="bai-res">Bairro:</label>
                    <input type="text" id="bai-res" name="bai-res"><br>
                </fieldset>
                <fieldset>
                    <legend>Dados comerciais</legend>
                    <label for="cep-com">CEP:</label>
                    <input type="text" id="cep-com" name="cep-com"><br>
                    <label for="raz-soc">Razão Social:</label>
                    <input type="text" id="end-com" name="raz-soc"><br>
                    <label for="nom-fan">Nome Fantasia:</label>
                    <input type="text" id="end-com" name="end-com"><br>
                    <label for="url-com">URL:</label>
                    <input type="text" id="url-com" name="url-com"><br>
                    <label for="email-com">Email Comercial:</label>
                    <input type="text" id="email-com" name="email-com"><br>
                </fieldset>
                <button type="submit">Enviar</button>
            </form>
    </main>
</body>

</html>
```

#### Quais barreiras você idetificou?
Aluno | Barreira
-- | --
Jonas | Tamanho da fonte
Edmilson | Titulo mais explicativo
Edmilson | Linguagem
Diego | Linguagem pt-br
Allan | Elementos semanticos
Nicolas | Espaçamento do texto
Fabio | Divisao article, main...


#### Quais ajustes acessíveis poderíamos incluir no código?
Aluno | Barreira | Ajuste
-- | -- | --

